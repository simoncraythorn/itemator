# README #

This is a console app tool for bulk changing item status's for an event.  For example to change all item 'T-SHIRT' from A status to W status.

Written during the COVID-19 crisis of 2020 to assist with issuing refunds to attendees.
