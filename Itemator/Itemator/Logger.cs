﻿//--------------------------------------------------------------------------------
// <copyright file="Logger.cs" company="Convention Data Services">
//   Initial edit Mar 25 2020
// </copyright>
//--------------------------------------------------------------------------------

namespace Itemator
{
    using System;
    using System.IO;
    using System.Text;

    public class Logger
    {
        #region Fields
        private readonly StringBuilder logString;
        #endregion

        #region Constructor
        public Logger()
        {
            this.logString = new StringBuilder();
        }
        #endregion

        public void WriteLine(string text = null)
        {
            if (text != null)
            {
                Console.WriteLine(text);
                this.logString.Append(text).Append(Environment.NewLine);
            }
            else
            {
                Console.WriteLine();
                this.logString.Append(Environment.NewLine);
            }
        }

        public void Write(string text)
        {
            Console.Write(text);
            this.logString.Append(text);
        }

        public void SaveLog()
        {
            if (this.logString.Length > 0)
            {
                using (StreamWriter sw = new StreamWriter("results.txt"))
                {
                    sw.Write(this.logString.ToString());
                }
            }
        }
    }
}