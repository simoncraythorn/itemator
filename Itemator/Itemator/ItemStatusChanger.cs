﻿//--------------------------------------------------------------------------------
// <copyright file="ItemStatusChanger.cs" company="Convention Data Services">
//   Initial edit Mar 25 2020
// </copyright>
//--------------------------------------------------------------------------------

namespace Itemator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CdsBusinessObjects.Models.CdsAuth;
    using CdsBusinessObjects.Models.Event;
    using CdsRepositoryInterfaces.Event;

    public class ItemStatusChanger
    {
        #region Fields
        private readonly ITabOrderRepository tabOrderRepository;
        private readonly ITabItemRepository tabItemRepository;
        private readonly Logger logger;
        #endregion

        #region Constructor
        public ItemStatusChanger(ITabOrderRepository tabOrderRepository, ITabItemRepository tabItemRepository, Logger logger)
        {
            this.tabOrderRepository = tabOrderRepository;
            this.tabItemRepository = tabItemRepository;
            this.logger = logger;
        }
        #endregion
        public async Task DoStatusChange(EventDetails eventDetails, CdsAuth cdsAuth)
        {
            this.logger.WriteLine($"Starting item update process for event {eventDetails.EventCode} - item code {eventDetails.ItemCode}");
            this.logger.WriteLine($"changing status from {eventDetails.CurrentItemStatus} to {eventDetails.NewItemStatus}");
            if (eventDetails.IsTestMode)
            {
                this.logger.WriteLine(" - TEST MODE");
            }
            else
            {
                this.logger.WriteLine();
            }

            this.tabOrderRepository.SetEvent(eventDetails.EventCode);
            this.tabItemRepository.SetEvent(eventDetails.EventCode);

            List<TabOrder> tabOrders = await this.tabOrderRepository.GetAll(eventDetails.EventPk);
            List<TabItem> tabItems = await this.tabItemRepository.GetAll(eventDetails.EventPk);

            int ordersTotal = 0;
            int itemsUpdated = 0;

            foreach (TabOrder tabOrder in tabOrders)
            {
                if (tabOrder.OrderNumber >= eventDetails.StartOrderNumber && tabOrder.OrderNumber <= eventDetails.EndOrderNumber)
                {
                    List<TabItem> tabItemsForOrder = tabItems.Where(ti => ti.OrderNumber == tabOrder.OrderNumber && ti.ItemCode == eventDetails.ItemCode && ti.ItemStatus == eventDetails.CurrentItemStatus).ToList();
                    if (tabItemsForOrder.Any())
                    {
                        ordersTotal++;

                        foreach (TabItem tabItem in tabItemsForOrder)
                        {
                            if (!eventDetails.IsTestMode)
                            {
                                tabItem.ItemStatus = eventDetails.NewItemStatus;
                                tabItem.ItemNotes += $" - item status changed from {eventDetails.CurrentItemStatus} to {eventDetails.NewItemStatus} by itemator app";
                                tabItem.ModifiedBy = cdsAuth.Username;
                                tabItem.ModifiedDate = DateTime.Now.AddHours(3);
                                await this.tabItemRepository.Save(tabItem);
                            }
                        }

                        itemsUpdated += tabItemsForOrder.Count;

                        this.logger.WriteLine($"Order #{tabOrder.OrderNumber} - {tabItemsForOrder.Count} items updated");
                    }
                }
            }

            this.logger.WriteLine();
            this.logger.WriteLine($"{ordersTotal} orders processed.  {itemsUpdated} items successfully updated.");
        }
    }
}
