﻿//--------------------------------------------------------------------------------
// <copyright file="EventDetails.cs" company="Convention Data Services">
//   Initial edit Mar 25 2020
// </copyright>
//--------------------------------------------------------------------------------

namespace Itemator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CdsBusinessObjects.Models.Cds;
    using CdsBusinessObjects.Models.CdsAuth;
    using CdsBusinessObjects.Models.Event;
    using CdsRepositoryInterfaces.Cds;
    using CdsRepositoryInterfaces.Event;

    public class EventDetails
    {
        #region Fields
        private readonly IEventInfo2Repository eventInfo2Repository;
        private readonly ITabSsEventDefinitionRepository tabSsEventDefinitionRepository;
        private readonly ITabSsItemDefinitionsRepository tabSsItemDefinitionsRepository;
        #endregion

        #region Constructor
        public EventDetails(IEventInfo2Repository eventInfo2Repository, ITabSsEventDefinitionRepository tabSsEventDefinitionRepository, ITabSsItemDefinitionsRepository tabSsItemDefinitionsRepository)
        {
            this.eventInfo2Repository = eventInfo2Repository;
            this.tabSsEventDefinitionRepository = tabSsEventDefinitionRepository;
            this.tabSsItemDefinitionsRepository = tabSsItemDefinitionsRepository;
        }

        #region Properties
        public int EventPk { get; set; }
        public string EventCode { get; set; }
        public string ItemCode { get; set; }
        public string CurrentItemStatus { get; set; }
        public string NewItemStatus { get; set; }
        public bool IsTestMode { get; set; }
        public string PaymentProcessorParams { get; set; }
        public int StartOrderNumber { get; set; }
        public int EndOrderNumber { get; set; }
        #endregion

        #endregion
        public async Task<bool> SelectEvent(CdsAuth cdsAuth)
        {
            bool ret = false;

            Console.Write("Please enter the event code: ");
            string eventCode = Console.ReadLine();
            if (!string.IsNullOrEmpty(eventCode))
            {
                List<EventInfo2> eventInfo2List = await this.eventInfo2Repository.GetAllAsync(cdsAuth.ClientId, false);
                EventInfo2 eventInfo2 = eventInfo2List.FirstOrDefault(ei => ei.EventCode.ToLower() == eventCode.ToLower());
                if (eventInfo2 != null)
                {
                    Console.WriteLine(eventInfo2.Name);
                    Console.Write("Please confirm this is the event you want to refund by entering the event code again: ");
                    string eventCodeVerify = Console.ReadLine();
                    if (eventCodeVerify?.ToLower() == eventCode.ToLower())
                    {
                        this.tabSsEventDefinitionRepository.SetEvent(eventCode);
                        this.tabSsItemDefinitionsRepository.SetEvent(eventCode);
                        TabSsEventDefinition tabSsEventDefinition = await this.tabSsEventDefinitionRepository.Get();
                        this.EventPk = tabSsEventDefinition.Pk;
                        this.EventCode = eventCode;
                        this.PaymentProcessorParams = tabSsEventDefinition.PaymentProcessorParams;

                        Console.WriteLine();
                        Console.Write("Please enter the item code you wish to modify: ");
                        string itemCode = Console.ReadLine();

                        TabSsItemDefinitions item = await this.tabSsItemDefinitionsRepository.GetByItemCode(itemCode);
                        if (item != null)
                        {
                            Console.WriteLine($"{item.ItemCode} - {item.Name}");
                            Console.WriteLine();
                            Console.Write("Please enter the item code again to confirm this is the item you wish to modify: ");
                            itemCode = Console.ReadLine();
                            if (!string.IsNullOrEmpty(itemCode) && itemCode.ToLower() == item.ItemCode.ToLower())
                            {
                                this.ItemCode = item.ItemCode;

                                Console.WriteLine();
                                Console.Write("What must the status of the item be NOW (A/W): ");
                                this.CurrentItemStatus = Console.ReadLine();
                                if (!string.IsNullOrEmpty(this.CurrentItemStatus) && (this.CurrentItemStatus.ToUpper() == "A" || this.CurrentItemStatus.ToUpper() == "W"))
                                {
                                    this.CurrentItemStatus = this.CurrentItemStatus.ToUpper();

                                    Console.WriteLine();
                                    Console.Write($"Please enter the status code you wish the item to be changed to ({(this.CurrentItemStatus == "A" ? "W" : "A")}): ");
                                    this.NewItemStatus = Console.ReadLine();
                                    if (!string.IsNullOrEmpty(this.NewItemStatus) && ((this.CurrentItemStatus == "A" && (this.NewItemStatus == "W" || this.NewItemStatus == "w")) || (this.CurrentItemStatus == "W" && (this.NewItemStatus == "A" || this.NewItemStatus == "a"))))
                                    {
                                        this.NewItemStatus = this.NewItemStatus.ToUpper();

                                        Console.WriteLine();
                                        Console.WriteLine("Default mode is test mode.");
                                        Console.WriteLine("In this mode the report will be run only - no items will be modified and the database will not be updated.");
                                        Console.WriteLine();
                                        Console.Write("To run in live mode please type 'GO-ahead' (case sensitive) or press enter to continue in test mode: ");

                                        string goahead = Console.ReadLine();
                                        if (goahead != "GO-ahead")
                                        {
                                            this.IsTestMode = true;
                                        }

                                        do
                                        {
                                            Console.WriteLine();
                                            if (this.IsTestMode)
                                            {
                                                Console.Write("[Test Mode] ");
                                            }

                                            Console.Write("Start at order #:");
                                            string start = Console.ReadLine();
                                            if (int.TryParse(start, out int startInt))
                                            {
                                                this.StartOrderNumber = startInt;
                                                break;
                                            }

                                            Console.WriteLine("Not a valid number - try again");
                                        }
                                        while (true);

                                        do
                                        {
                                            if (this.IsTestMode)
                                            {
                                                Console.Write("[Test Mode] ");
                                            }

                                            Console.Write("End at order #:");
                                            string end = Console.ReadLine();
                                            if (int.TryParse(end, out int endInt))
                                            {
                                                this.EndOrderNumber = endInt;
                                                break;
                                            }

                                            Console.WriteLine("Not a valid number - try again");
                                        }
                                        while (true);

                                        ret = true;
                                    }
                                    else
                                    {
                                        Console.WriteLine("This is not a valid status for this tool");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("That is not a valid status for this tool");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Item code did not match");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Item not found");
                        }
                    }
                    else
                    {
                        Console.WriteLine("You did not enter the same event code");
                    }
                }
                else
                {
                    Console.WriteLine("Event not found");
                }
            }
            else
            {
                Console.WriteLine("No event selected");
            }

            return ret;
        }
    }
}
