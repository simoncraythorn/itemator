﻿//--------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Convention Data Services">
//   Initial edit Mar 25 2020
// </copyright>
//--------------------------------------------------------------------------------

namespace Itemator
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using CdsBusinessObjects.Models.CdsAuth;
    using CdsV1DataClient;
    using CdsV1DataClient.CdsAuth;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Program
    {
        public static async Task Main(string[] args)
        {
#if DEBUG
            IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true);
#else
            IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName).AddJsonFile("appsettings.release.json", true, true);
#endif

            IConfigurationRoot configuration = builder.Build();

            CdsV1DataServiceConfig cdsV1DataServiceConfig = configuration.GetSection("CdsV1DataServiceConfig").Get<CdsV1DataServiceConfig>();
            CdsAuthConfig cdsAuthConfig = configuration.GetSection("CdsAuthConfig").Get<CdsAuthConfig>();
            Logger logger = new Logger();

            ServiceProvider serviceProvider = new ServiceCollection().AddCdsV1DataClient(cdsAuthConfig, cdsV1DataServiceConfig).AddTransient<Login>().AddTransient<EventDetails>().AddTransient<ItemStatusChanger>().AddSingleton(logger).BuildServiceProvider();

            Console.WriteLine("This utility will change the status of every matching item");
            Console.WriteLine("in all selected orders to the selected status - USE WITH **EXTREME** CAUTION");
            Console.WriteLine();

            Login login = serviceProvider.GetService<Login>();
            CdsAuth cdsAuth = await login.DoLogin();
            if (cdsAuth != null && cdsAuth.IsValidated)
            {
                Console.WriteLine();
                EventDetails eventDetails = serviceProvider.GetService<EventDetails>();
                if (await eventDetails.SelectEvent(cdsAuth))
                {
                    Console.WriteLine();
                    ItemStatusChanger itemStatusChanger = serviceProvider.GetService<ItemStatusChanger>();
                    await itemStatusChanger.DoStatusChange(eventDetails, cdsAuth);

                    logger.SaveLog();
                }
            }
            else
            {
                Console.WriteLine("Incorrect login.");
            }

            Console.WriteLine("Press Any Key to close");
            Console.ReadKey();
        }
    }
}
