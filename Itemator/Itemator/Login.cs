﻿//--------------------------------------------------------------------------------
// <copyright file="Login.cs" company="Convention Data Services">
//   Initial edit Mar 25 2020
// </copyright>
//--------------------------------------------------------------------------------

namespace Itemator
{
    using System;
    using System.Threading.Tasks;
    using CdsBusinessObjects.Models.Cds;
    using CdsBusinessObjects.Models.CdsAuth;
    using CdsRepositoryInterfaces.Cds;
    using CdsRepositoryInterfaces.CdsAuth;

    public class Login
    {
        #region Fields
        private readonly ICdsAuthRepository cdsAuthRepository;
        private readonly IAccountInfoRepository accountInfoRepository;
        #endregion

        #region Constructor
        public Login(ICdsAuthRepository cdsAuthRepository, IAccountInfoRepository accountInfoRepository)
        {
            this.cdsAuthRepository = cdsAuthRepository;
            this.accountInfoRepository = accountInfoRepository;
        }
        #endregion

        public async Task<CdsAuth> DoLogin()
        {
            Console.Write("Username: ");
            string username = Console.ReadLine();
            Console.Write("Password: ");
            string password = this.ReadPassword();
            Console.WriteLine();

            CdsAuth cdsAuth = await this.cdsAuthRepository.Get(username, password);
            if (cdsAuth.IsValidated)
            {
                AccountInfo accountInfo = await this.accountInfoRepository.Get(username);
                if (accountInfo != null)
                {
                    cdsAuth.AllowCredits = accountInfo.AllowCredits == 1;
                    cdsAuth.ClientId = accountInfo.ClientId;
                }
            }

            return cdsAuth;
        }

        private string ReadPassword()
        {
            string password = string.Empty;

            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Enter || key.Key == ConsoleKey.Tab)
                {
                    break;
                }

                if (key.Key == ConsoleKey.Backspace)
                {
                    if (password.Length > 0)
                    {
                        password = password.Substring(0, password.Length - 1);
                    }
                }
                else
                {
                    password += key.KeyChar;
                }
            }

            return password;
        }
    }
}
